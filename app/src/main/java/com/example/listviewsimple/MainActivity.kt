package com.example.listviewsimple

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Adapter
import android.widget.ArrayAdapter
import android.widget.ListView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var listdat: ListView = findViewById(R.id.lista)
        var dato: ArrayList<String> = ArrayList()

        dato.add("Pepsi")
        dato.add("Cocacola")
        dato.add("Bigcola")
        dato.add("poweride")
        dato.add("Jugos")
        dato.add("Jumex")
        dato.add("Sabritas")
        dato.add("Papas fritas")
        dato.add("Amburguesa")
        dato.add("Galletas")
        dato.add("Yogourt")
        dato.add("Helados")
        dato.add("Bigcola")
        dato.add("poweride")
        dato.add("Jugos")
        dato.add("Jumex")
        dato.add("Sabritas")
        dato.add("Bigcola")
        dato.add("poweride")
        dato.add("Jugos")
        dato.add("Jumex")
        dato.add("Sabritas")
        dato.add("Papas fritas")
        dato.add("Amburguesa")
        dato.add("Galletas")
        dato.add("Yogourt")
        dato.add("Bigcola")
        dato.add("poweride")
        dato.add("Jugos")
        dato.add("Jumex")
        dato.add("Sabritas")
        dato.add("Papas fritas")
        dato.add("Amburguesa")
        dato.add("Galletas")
        dato.add("Yogourt")
        val adaptador = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, dato)
        listdat.adapter = adaptador

    }
}